﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace baitap1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Sinhvien> danhsachsSinhviens = new List<Sinhvien>();
            int n;
            Console.Write("Nhap so SV: ");
            do
            {
                n = Convert.ToInt32(Console.ReadLine());
                if (n <= 0)
                {
                    Console.WriteLine("Error! So SV phai lon hon 0! \n");
                    Console.Write("Nhap lai so SV: \n");
                }
                else break;
            } while (n > 0);

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Nhap thong tin cua sinh vien thu {0} ", (i + 1));
                Sinhvien sv = new Sinhvien();
                sv.nhapThongtin();
                danhsachsSinhviens.Add(sv);
            }

            Console.WriteLine("\n");

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Thong tin cua sinh vien thu {0} ", (i + 1));
                danhsachsSinhviens[i].xuatthongtin();
            }

            Sinhvien giamdan;
            for (int i = 0; i < n; i++)
            {
                for (int j = i + 1; j < n; j++)
                {
                    if (danhsachsSinhviens[i].DiemTrungbinh(danhsachsSinhviens[i].Diemtoan,
                            danhsachsSinhviens[i].Diemly, danhsachsSinhviens[i].Diemhoa) <
                        danhsachsSinhviens[j].DiemTrungbinh(danhsachsSinhviens[j].Diemtoan,
                            danhsachsSinhviens[j].Diemly, danhsachsSinhviens[j].Diemhoa))
                    {
                        giamdan = danhsachsSinhviens[i];
                        danhsachsSinhviens[i] = danhsachsSinhviens[j];
                        danhsachsSinhviens[j] = giamdan;
                    }
                }
            }

            Console.WriteLine("\n Danh sach 3 sinh vien co diem trung binh cao nhat giam dan");
        
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("HoTen: {0},Diem Trung binh: {1}", danhsachsSinhviens[i].Hoten,
                    danhsachsSinhviens[i].DiemTrungbinh(danhsachsSinhviens[i].Diemtoan, danhsachsSinhviens[i].Diemly,
                        danhsachsSinhviens[i].Diemhoa));
            }

            Console.ReadKey();
        }
    }
}