﻿using System;

namespace baitap1
{
    public class Sinhvien
    {
        private string hoten;
        private float diemtoan, diemly, diemhoa;

        public string Hoten
        {
            get => hoten;
            set => hoten = value;
        }

        public float Diemtoan
        {
            get => diemtoan;
            set => diemtoan = value;
        }

        public float Diemly
        {
            get => diemly;
            set => diemly = value;
        }

        public float Diemhoa
        {
            get => diemhoa;
            set => diemhoa = value;
        }

        public void nhapThongtin()
        {
            Console.Write("nhap ten: ");
            hoten = Console.ReadLine();
            Console.Write("nhap diem toan: ");
            do
            {
                diemtoan = float.Parse(Console.ReadLine());
                if (diemtoan > 10 || diemtoan < 0)
                {
                    Console.WriteLine("Error! Diem Toan phai nam trong 0-10");
                    Console.Write("Nhap lai Diem Toan:");
                }
                else break;
            } while (diemtoan < 10 || diemtoan > 0);
            Console.Write("nhap diem ly: ");
            do
            {
                diemly = float.Parse(Console.ReadLine());
                if (diemly > 10 || diemly < 0)
                {
                    Console.WriteLine("Error! Diem Ly phai nam trong 0-10");
                    Console.Write("Nhap lai Diem Ly:");
                }
                else break;
            } while (diemly < 10 || diemly > 0);
            Console.Write("nhap diem hoa: ");
            do
            {
                diemhoa = float.Parse(Console.ReadLine());
                if (diemhoa > 10 || diemhoa < 0)
                {
                    Console.WriteLine("Error! Diem Hoa phai nam trong 0-10");
                    Console.Write("Nhap lai Diem Hoa:");
                }
                else break;
            } while (diemhoa < 10 || diemhoa > 0);
        }

        public float DiemTrungbinh(float diemtoan, float diemly, float diemhoa)
        {
            return (diemtoan + diemly + diemhoa) / 3;
        }

        public void xuatthongtin()
        {
            Console.WriteLine("Ho ten: {0} , Diem Toan: {1} , Diem Ly: {2} , Diem Hoa: {3} , Diem Trung binh: {4}",
                hoten, diemtoan, diemly, diemhoa, DiemTrungbinh(diemtoan, diemly, diemhoa));
        }
    }
}